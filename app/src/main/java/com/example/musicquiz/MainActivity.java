package com.example.musicquiz;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;
    private int correctSong;
    private int userScore = 0;
    private int roundScore = 0;
    private MediaPlayer mediaPlayer;
    private ArrayList<Song> songs;
    private CountDownTimer roundTimer;
    private int roundLimit = 10;
    private int roundNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songs = getUserMedia();

        initialiseMediaPlayer();

        initialiseTimer();

        newRound(0);

    }

    protected void initialiseTimer() {
        roundTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                //mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                roundScore -= 1;
            }

            public void onFinish() {
                //mTextField.setText("done!");
                //roundScore = 0;
                newRound(0);
            }
        };
    }

    protected void endGame() {
        Toast.makeText(MainActivity.this,"Final Score: " + userScore, Toast.LENGTH_SHORT).show();
    }

    protected void newRound(int lastRoundScore) {
        roundNumber += 1;
        roundTimer.cancel();
        userScore += lastRoundScore;
        Toast.makeText(MainActivity.this,"Score: " + userScore + "(" + lastRoundScore + ")", Toast.LENGTH_SHORT).show();
        mediaPlayer.reset();
        if (roundNumber > roundLimit) {
            endGame();
        } else {

            roundTimer.start();
            roundScore = 31;

            Button button1 = (Button)findViewById(R.id.button1);
            Button button2 = (Button)findViewById(R.id.button2);
            Button button3 = (Button)findViewById(R.id.button3);
            Button button4 = (Button)findViewById(R.id.button4);

            ArrayList<Song> songs = new ArrayList<Song>();
            for(int i = 0; i < 4; i++) {
                songs.add(getRandomSong());
            }

            button1.setText(songs.get(0).artist + " - " + songs.get(0).title);
            button2.setText(songs.get(1).artist + " - " + songs.get(1).title);
            button3.setText(songs.get(2).artist + " - " + songs.get(2).title);
            button4.setText(songs.get(3).artist + " - " + songs.get(3).title);
            Random rand = new Random();
            correctSong = rand.nextInt(4);

            try {
                mediaPlayer.setDataSource(getApplicationContext(), songs.get(correctSong).uri);
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            int position = getRandomPosition(mediaPlayer);
            mediaPlayer.seekTo(position);

            mediaPlayer.start();
        }

    }

    protected void initialiseMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                //Toast.makeText(MainActivity.this,"Incorrect. :) Score: " + userScore, Toast.LENGTH_SHORT).show();
                //userScore = 0;
                newRound(0);
            }
        });
    }

    protected void onAnswerSelect(View v) {

        int score = 0;

        switch(v.getId())
        {
            case R.id.button1 :
                if(correctSong == 0) {
                    score = roundScore;
                }
                break;
            case R.id.button2 :
                if(correctSong == 1) {
                    score = roundScore;
                }
                break;
            case R.id.button3 :
                if(correctSong == 2) {
                    score = roundScore;
                }
                break;
            case R.id.button4 :
                if(correctSong == 3) {
                    score = roundScore;
                }
                break;
            default :
                //Toast.makeText(MainActivity.this,"Something went wrong. :(", Toast.LENGTH_SHORT).show();
                break;
        }
        Log.d("DEBUG", "onAnswerSelect: Answer selected");
        newRound(score);
    }

    protected int getRandomPosition(MediaPlayer mediaPlayer) {
        Random rand = new Random();

        int position = rand.nextInt(mediaPlayer.getDuration() - 30000);
        Log.d("DEBUG", "getRandomPosition: " + position);
        if (position < 0) {position = 0;}
        Log.d("DEBUG", "getRandomPosition: " + position);

        return position;
    }

    protected Song getRandomSong() {

        Random rand = new Random();

        //Uri contentUri = ContentUris.withAppendedId(
                //android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);

        Song song = songs.get(rand.nextInt(songs.size()));
        long id = song.id;
        song.uri = ContentUris.withAppendedId(
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, song.uri);
        String stringDuration
                = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        song.title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        song.artist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        song.album = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        retriever.release(); //don't forget to call this when done

        song.duration = Integer.parseInt(stringDuration);

        if (song.duration < 30000) {
            song = getRandomSong();
        }

        return song;
    }

    protected ArrayList<Song> getUserMedia() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

        return generateMediaList();
    }

    protected ArrayList<Song> generateMediaList() {
        ArrayList<Song> songs = new ArrayList<Song>();

        ContentResolver contentResolver = getContentResolver();
        Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(uri, null, null, null, null);
        if (cursor == null) {
            // query failed, handle error.
        } else if (!cursor.moveToFirst()) {
            // no media on the device
        } else {
            int titleColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
            do {
                long thisId = cursor.getLong(idColumn);
                String thisTitle = cursor.getString(titleColumn);
                // ...process entry...
                Song thisSong = new Song();
                thisSong.id = thisId;
                thisSong.title = thisTitle;
                songs.add(thisSong);
                Log.d("DEBUG", "generateMediaList: " + thisId + " " + thisTitle);
            } while (cursor.moveToNext());
        }

        return songs;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // storage-related task you need to do.
                    //generateMediaList();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
