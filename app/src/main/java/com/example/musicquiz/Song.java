package com.example.musicquiz;

import android.net.Uri;

public class Song {
    long id;
    Uri uri;
    int duration;
    String title;
    String artist;
    String album;
}
